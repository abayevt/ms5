package az.baku.ms.loanms.model;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "loan")
public class Loan {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private Double amount;
}
